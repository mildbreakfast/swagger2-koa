"use strict";
// router.spec.ts
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
/*
 The MIT License

 Copyright (c) 2014-2017 Carl Ansley

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */
const assert = require("assert");
const agent = require("supertest-koa-agent");
const router_1 = require("./router");
describe('router', () => {
    // noinspection ReservedWordAsName
    const document = {
        swagger: '2.0',
        info: {
            title: 'mock',
            version: '0.0.1'
        },
        basePath: '/mock',
        paths: {
            '/ping': {
                get: {
                    responses: {
                        200: {
                            description: '',
                            schema: {
                                type: 'object',
                                required: ['time'],
                                properties: {
                                    time: {
                                        type: 'string',
                                        format: 'date-time'
                                    }
                                }
                            }
                        }
                    }
                },
                head: {
                    responses: {
                        200: {
                            description: ''
                        }
                    }
                },
                patch: {
                    responses: {
                        204: {
                            description: ''
                        }
                    }
                },
                put: {
                    responses: {
                        204: {
                            description: ''
                        }
                    }
                },
                post: {
                    responses: {
                        201: {
                            description: ''
                        }
                    }
                },
                options: {
                    responses: {
                        204: {
                            description: ''
                        }
                    }
                },
                delete: {
                    responses: {
                        204: {
                            description: ''
                        }
                    }
                }
            },
            '/badPing': {
                get: {
                    responses: {
                        200: {
                            description: '',
                            schema: {
                                type: 'object',
                                required: ['time'],
                                properties: {
                                    time: {
                                        type: 'string',
                                        format: 'date-time'
                                    }
                                }
                            }
                        }
                    }
                },
                put: {
                    responses: {
                        201: { description: '' }
                    }
                },
                post: {
                    responses: {
                        201: { description: '' }
                    }
                }
            }
        }
    };
    const router = router_1.default(document);
    router.head('/ping', (context) => __awaiter(this, void 0, void 0, function* () {
        context.status = 200;
    }));
    router.get('/ping', (context, next) => __awaiter(this, void 0, void 0, function* () {
        context.status = 200;
        return next();
    }), (context) => __awaiter(this, void 0, void 0, function* () {
        context.body = {
            time: new Date().toISOString()
        };
    }));
    router.put('/ping', (context) => __awaiter(this, void 0, void 0, function* () {
        context.status = 204;
    }));
    router.patch('/ping', (context) => __awaiter(this, void 0, void 0, function* () {
        context.status = 204;
    }));
    router.post('/ping', (context) => __awaiter(this, void 0, void 0, function* () {
        context.status = 201;
    }));
    router.del('/ping', (context) => __awaiter(this, void 0, void 0, function* () {
        context.status = 204;
    }));
    router.get('/badPing', (context) => __awaiter(this, void 0, void 0, function* () {
        context.status = 200;
        context.body = {
            badTime: 'mock'
        };
    }));
    router.put('/badPing', (context) => __awaiter(this, void 0, void 0, function* () {
        context.status = 201;
        context.body = {
            something: 'mock'
        };
    }));
    router.post('/badPing', () => __awaiter(this, void 0, void 0, function* () {
        const err = new Error();
        err.status = 400;
        throw err;
    }));
    const http = agent(router.app());
    it('fails with invalid filename', () => {
        assert.throws(() => router_1.default('invalid.yml'), Error);
    });
    it('fails with invalid Swagger document', () => {
        assert.throws(() => router_1.default(__dirname + '/../.travis.yml'));
    });
    it('invalid path', () => __awaiter(this, void 0, void 0, function* () { return http.post('/mock/pingy').expect(404); }));
    it('invalid path', () => __awaiter(this, void 0, void 0, function* () { return http.post('/pingy').expect(404); }));
    it('invalid method', () => __awaiter(this, void 0, void 0, function* () { return http.patch('/mock/badPing').expect(405); }));
    it('invalid request', () => __awaiter(this, void 0, void 0, function* () { return http.get('/mock/ping?x=y').expect(400); }));
    it('validates valid GET operation', () => __awaiter(this, void 0, void 0, function* () {
        const { body } = yield http.get('/mock/ping').expect(200);
        assert.doesNotThrow(() => new Date(body.time));
    }));
    it('validates valid HEAD operation', () => __awaiter(this, void 0, void 0, function* () {
        const { body } = yield http.head('/mock/ping').expect(200);
        assert.deepStrictEqual(body, {});
    }));
    it('validates valid POST operation', () => __awaiter(this, void 0, void 0, function* () {
        const { body } = yield http.post('/mock/ping').expect(201);
        assert.deepStrictEqual(body, {});
    }));
    it('validates valid PATCH operation', () => __awaiter(this, void 0, void 0, function* () {
        const { body } = yield http.patch('/mock/ping').expect(204);
        assert.deepStrictEqual(body, {});
    }));
    it('validates valid DELETE operation', () => __awaiter(this, void 0, void 0, function* () {
        const { body } = yield http.del('/mock/ping').expect(204);
        assert.deepStrictEqual(body, {});
    }));
    it('support CORS via OPTIONS operation', () => __awaiter(this, void 0, void 0, function* () {
        const { body } = yield http.options('/mock/ping').expect(204);
        assert.deepStrictEqual(body, {});
    }));
    it('validates valid PUT operation', () => __awaiter(this, void 0, void 0, function* () {
        const { body } = yield http.put('/mock/ping').expect(204);
        assert.deepStrictEqual(body, {});
    }));
    it('handles POST operation throwing 400 error', () => __awaiter(this, void 0, void 0, function* () {
        yield http.post('/mock/badPing').expect(400);
    }));
    it('does not validate invalid operation response', () => __awaiter(this, void 0, void 0, function* () {
        const { body } = yield http.get('/mock/badPing').expect(500);
        assert.deepStrictEqual(body, {
            code: 'SWAGGER_RESPONSE_VALIDATION_FAILED',
            errors: [{
                    actual: { badTime: 'mock' },
                    expected: {
                        schema: { type: 'object', required: ['time'], properties: { time: { type: 'string', format: 'date-time' } } }
                    },
                    where: 'response',
                    error: 'data.time is required'
                }]
        });
    }));
    it('does not validate response where nothing is expected', () => __awaiter(this, void 0, void 0, function* () {
        const { body } = yield http.put('/mock/badPing').expect(500);
        assert.deepStrictEqual(body, {
            code: 'SWAGGER_RESPONSE_VALIDATION_FAILED',
            errors: [{
                    actual: { something: 'mock' },
                    where: 'response'
                }]
        });
    }));
});
//# sourceMappingURL=router.spec.js.map