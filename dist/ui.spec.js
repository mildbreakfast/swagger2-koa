"use strict";
// ui.spec.ts
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
/*
 The MIT License

 Copyright (c) 2014-2017 Carl Ansley

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */
const assert = require("assert");
const Koa = require("koa");
const agent = require("supertest-koa-agent");
const ui_1 = require("./ui");
const document = {
    swagger: '2.0',
    info: {
        title: 'mock',
        version: '0.0.1'
    },
    paths: {}
};
function getRequestClient(pathRoot, skipPaths) {
    const koa = new Koa().use(ui_1.default(document, pathRoot, skipPaths));
    return agent(koa);
}
describe('ui', () => {
    it('serves custom index.html', () => __awaiter(this, void 0, void 0, function* () {
        yield getRequestClient().get('/').expect(200);
    }));
    it('serves custom index.html from custom path root', () => __awaiter(this, void 0, void 0, function* () {
        yield getRequestClient('/swagger2').get('/swagger2').expect(200);
    }));
    it('serves files from non-skipped paths', () => __awaiter(this, void 0, void 0, function* () {
        yield getRequestClient('/swagger2', ['/swagger/images']).get('/swagger2/css/print.css').expect(200);
    }));
    it('does not serve files from skipped paths', () => __awaiter(this, void 0, void 0, function* () {
        yield getRequestClient('/swagger2', ['/swagger2/css']).get('/swagger2/css/print.css').expect(404);
    }));
    it('serves api-docs', () => __awaiter(this, void 0, void 0, function* () {
        const { body } = yield getRequestClient().get('/api-docs').expect(200);
        assert.deepStrictEqual(body, document);
    }));
    it('serves api-docs from custom path root', () => __awaiter(this, void 0, void 0, function* () {
        const { body } = yield getRequestClient('/swagger2').get('/swagger2/api-docs').expect(200);
        assert.deepStrictEqual(body, document);
    }));
    it('serves swagger UI', () => __awaiter(this, void 0, void 0, function* () { return getRequestClient().get('/swagger-ui.js').expect(200); }));
    it('serves swagger UI from custom path root', () => __awaiter(this, void 0, void 0, function* () {
        yield getRequestClient('/swagger2').get('/swagger2/swagger-ui.js').expect(200);
    }));
    it('does not serve files from paths outside path root', () => __awaiter(this, void 0, void 0, function* () {
        yield getRequestClient('/swagger2', ['/swagger/css']).get('/css/print.css').expect(404);
    }));
});
//# sourceMappingURL=ui.spec.js.map